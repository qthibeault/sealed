import functools


class SealedException(Exception):
    pass


def sealed(cls):
    """Seal a class so that it cannot be sub-classed outside of the module it is defined in."""

    @functools.wraps(cls.__new__)
    def sealed_wrapper(kls, *args, **kwargs):
        if kls._sealed and kls.__module__ != cls.__module__:
            raise SealedException

        return super(cls, kls).__new__(kls, *args, **kwargs)

    cls._sealed = True
    cls.__new__ = sealed_wrapper

    return cls
