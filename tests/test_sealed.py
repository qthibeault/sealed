import pytest
import sealed
from sealed.decorator import SealedException

from .classes import SealedClass, DerivedClass
from .classes import AbstractSealed, AbstractDerived

class IllegalClass(SealedClass):
    pass

class IllegalAbstract(AbstractSealed):
    def frobnicate():
        return

def test_sealed():
    sealed_instance = SealedClass()
    assert isinstance(sealed_instance, SealedClass)

    derived_instace = DerivedClass()
    assert isinstance(derived_instace, DerivedClass)

    with pytest.raises(sealed.SealedException):
        illegal_instance = IllegalClass()


def test_abstract_sealed():
    with pytest.raises(TypeError):
        abstract_instance = AbstractSealed()

    derived_instance = AbstractDerived()
    assert isinstance(derived_instance, AbstractDerived)

    with pytest.raises(SealedException):
        illegal_instance = IllegalAbstract()
