from abc import ABC, abstractmethod

from sealed import sealed

@sealed
class SealedClass:
    pass


class DerivedClass(SealedClass): 
    pass


@sealed
class AbstractSealed(ABC):
    @abstractmethod
    def frobnicate():
        raise NotImplementedError()


class AbstractDerived(AbstractSealed):
    def frobnicate():
        return
